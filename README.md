# Applications of GAN in Data Augmentation

## Abstract

The demand for analytical tools capable to interpreting patterns in large amount of data continues to grow at a rapid rate in the era of big data. As the size of the data sets continue to grow, the training times continue to increase. It is even becoming more challenging to design predictive model in presence of 
class imbalance using traditional machine learning algorithms in the big data domain. Since generative adversarial networks (GANs) are powerful generative models that have led to breakthroughs in image generation, it would be interesting to explore this powerful tool to improve the classification
performance in the presence of class imbalance. In this project, we have used deep convolutional GAN (DCGAN)
 and conditional generator-based conditional tabular GAN (CTGAN) models to generate both image and tabular samples to improve the performance of the random forest and neural network classifiers. We
also compared our data augmentation performance with the SMOTE resampling technique. Data augmentation performance are measured on the credit card, CIFAR10, and MNIST hand written digit datasets to analyze the impact of data augmentation. We saw that the GAN models did well on the image data sets compared to tabular data. 